import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import MinMaxScaler
import time

# Constants
RANDOM_STATE=24
# SIZE=5000
MAX_ITER=10000

# Dataset: https://www.kaggle.com/balaka18/email-spam-classification-dataset-csv
df = pd.read_csv('./emails.csv')

print('Benchmarking classifier models on spams detection.')
print('------------------------------------------------------')

# link: https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html#sklearn.preprocessing.MinMaxScaler.fit_transform
scaler = MinMaxScaler()

X = df[df.keys()[1:-1].values].values # [:SIZE]
# Normalize the data
X = scaler.fit_transform(X)
y = df['Prediction'].values # [:SIZE]

# print(df.head(1))
# print(X[0])

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=RANDOM_STATE)

# ----------------------------------------------------------------------------
# Models
# 1. Logistic regression
# link: https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression
lr = LogisticRegression(max_iter=MAX_ITER)
print('Training logistic regression..')
start = time.time()
lr.fit(X_train, y_train)
print(f'Done in {round(time.time()-start, 2)}s\n')

# 2. Random Forest
# link: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
rf = RandomForestClassifier(random_state=RANDOM_STATE)
print('Training random forest..')
start = time.time()
rf.fit(X_train, y_train)
print(f'Done in {round(time.time()-start, 2)}s\n')

# 3. Neutral network
# link: https://scikit-learn.org/stable/modules/neural_networks_supervised.html#multi-layer-perceptron
mlp = MLPClassifier(solver='lbfgs', max_iter=MAX_ITER, alpha=1e-5, hidden_layer_sizes=(128), random_state=RANDOM_STATE)
print('Training neural network..')
start = time.time()
mlp.fit(X_train, y_train)
print(f'Done in {round(time.time()-start, 2)}s\n')

# 4. Naive Bayes
nb = GaussianNB()
print('Training Gaussian Naive Bayes..')
start = time.time()
nb.fit(X_train, y_train)
print(f'Done in {round(time.time()-start, 2)}s\n')

# ----------------------------------------------------------------------------
# Print the accuracies
print('Results:')
print('Accuracy of logistic regression:', lr.score(X_test, y_test))
print('Accuracy of random forest:', rf.score(X_test, y_test))
print('Accuracy of multi layer neural network:', mlp.score(X_test, y_test))
print('Accuracy of gaussian naive bayes:', nb.score(X_test, y_test)) 

# ----------------------------------------------------------------------------
# Predictions
print('\nPredictions:')
print('Email:', X_test[0])
print('Prediction of logistic regression:', lr.predict([X_test[0]])[0])
print('Prediction of random forest:', rf.predict([X_test[0]])[0])
print('Prediction of neural network:', mlp.predict([X_test[0]])[0])
print('Prediction of gaussian naive bayes:', nb.predict([X_test[0]])[0])
print('Real:', y_test[0])
